//Teste baseado no unit test que est� presente na p�gina do unit test do desafio.
package test;

import org.junit.Test;

import main.CheckOut;
import main.SpecialPrice;
import main.SpecialPriceRuleSet;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CheckOutTest {
    public int calculatePrice(String goods) {
        CheckOut co = new CheckOut(givenPriceRuleSet());
        for(int i=0; i<goods.length(); i++) {
            co.scan(String.valueOf(goods.charAt(i)));
        }
        return co.total();
    }

    //Definir os precos de cada item.
    private SpecialPriceRuleSet givenPriceRuleSet() {
        Map<String, SpecialPrice> priceRules = new HashMap<>();
        priceRules.put("A", new SpecialPrice(50, 3, 130));
        priceRules.put("B", new SpecialPrice(30, 2, 45));
        priceRules.put("C", new SpecialPrice(20));
        priceRules.put("D", new SpecialPrice(15));
        return new SpecialPriceRuleSet(priceRules);
        
    }
    @Test
    public void totals() {
        assertEquals(0, calculatePrice(""));
        assertEquals(50, calculatePrice("A"));
        assertEquals(80, calculatePrice("AB"));
        assertEquals(115, calculatePrice("CDBA"));
        assertEquals(100, calculatePrice("AA"));
        assertEquals(130, calculatePrice("AAA"));
        assertEquals(180, calculatePrice("AAAA"));
        assertEquals(230, calculatePrice("AAAAA"));
        assertEquals(260, calculatePrice("AAAAAA"));
        assertEquals(160, calculatePrice("AAAB"));
        assertEquals(175, calculatePrice("AAABB"));
        assertEquals(190, calculatePrice("AAABBD"));
        assertEquals(190, calculatePrice("DABABA"));
    }

    @Test
    public void incremental() {
        CheckOut co = new CheckOut(givenPriceRuleSet());
        assertEquals(0, co.total());
        co.scan("A"); assertEquals(50, co.total());
        co.scan("B"); assertEquals(80, co.total());
        co.scan("A"); assertEquals(130, co.total());
        co.scan("A"); assertEquals(160, co.total());
        co.scan("B"); assertEquals(175, co.total());
    }

}